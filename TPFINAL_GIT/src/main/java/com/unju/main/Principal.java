package com.unju.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Persona> listaPersonas = new ArrayList<>();

        int opcion;

        do {
            System.out.println("Menu de Opciones:");
            System.out.println("1. Agregar Personas");
            System.out.println("2. Mostrar Personas");
            System.out.println("3. Borrar Personas");
            System.out.println("4. Salir");
            System.out.print("Ingrese la opción deseada: ");

            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                   // agregarPersonas(scanner, listaPersonas);
                    break;
                case 2:
                   // mostrarPersonas(listaPersonas);
                    break;
                case 3:
                   // borrarPersonas(scanner, listaPersonas);
                    break;
                case 4:
                    System.out.println("Saliendo del programa. ¡Hasta luego!");
                    break;
                default:
                    System.out.println("Opción no válida. Intente de nuevo.");
            }

        } while (opcion != 4);
    }


private static void agregarPersonas(Scanner scanner, ArrayList<Persona> listaPersonas) {
    System.out.print("Ingrese el nombre de la persona: ");
    String nombre = scanner.next();
    System.out.print("Ingrese el apellido de la persona: ");
    String apellido = scanner.next();
    System.out.print("Ingrese el teléfono de la persona: ");
    String telefono = scanner.next();

    Persona nuevaPersona = new Persona(nombre, apellido, telefono);
    listaPersonas.add(nuevaPersona);

    System.out.println("Persona agregada exitosamente.");
}

}
